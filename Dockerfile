#FROM node:13.5.0-alpine
#RUN apk update && apk add --no-cache make git
#WORKDIR /app
#COPY package.json yarn.lock /app/
#RUN yarn
#COPY . /app
#RUN yarn build


FROM node:13.5.0-alpine AS builder
WORKDIR /app
COPY . /app
RUN yarn run build

FROM node:13.5.0-alpine
RUN yarn global add serve
WORKDIR /app
COPY --from=builder /app/build .